# Use the official gradle image to create a build artifact.
FROM eclipse-temurin:21-jre as builder

# Copy local code to the container image.
WORKDIR /app
COPY build.gradle.kts settings.gradle.kts gradlew ./
COPY gradle gradle
COPY src src

# Build a release artifact.
RUN ./gradlew clean build -x test

# Use AdoptOpenJDK for base image.
FROM eclipse-temurin:21-jre

# Copy the jar to the production image from the builder stage.
COPY --from=builder /app/build/libs/simple_api.jar /simple_api.jar

# Run the web service on container startup.
CMD ["java", "-server", "-Xmx512m", "-Xss512k", "-XX:CICompilerCount=2", "-Dfile.encoding=UTF-8", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/simple_api.jar"]