plugins {
    id("io.ktor.plugin") version "2.3.7"
    kotlin("jvm") version "1.9.21"
}

group = "org.simple.api"
version = "1.0-SNAPSHOT"

application {
    mainClass.set("org.simple.api.ApplicationKt")
}

ktor {
    fatJar {
        archiveFileName.set("simple_api.jar")
    }
}

repositories {
    mavenCentral()
}

var ktorVersion = "3.0.0-beta-1"

dependencies {
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-serialization-jackson:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-serialization:$ktorVersion")
    implementation("io.ktor:ktor-server-test-host:$ktorVersion")
    implementation("io.ktor:ktor-client-content-negotiation:2.2.4")
    implementation("io.ktor:ktor-client-core:2.2.4")
    implementation("io.ktor:ktor-client-serialization:2.2.4")
    implementation("ch.qos.logback:logback-classic:1.4.7")

    testImplementation("org.jetbrains.kotlin:kotlin-test:1.9.22")
    implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

kotlin {
    jvmToolchain(21)
}