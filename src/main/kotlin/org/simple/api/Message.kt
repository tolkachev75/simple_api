package org.simple.api

import io.ktor.http.*
import io.ktor.serialization.jackson.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.logging.*


data class Message(val text: String)

val data = mutableListOf<Message>()
internal val LOGGER = KtorSimpleLogger("com.simple.api.message")

val RequestTracePlugin = createRouteScopedPlugin("RequestTracePlugin", { }) {
    onCall { call ->
        LOGGER.info("Processing call: ${call.request.uri}")
    }
}

fun Application.message() {
    install(ContentNegotiation) {
        jackson ()
    }
    install(RequestTracePlugin)

    routing {
        get("/") {
            call.respondText("Hello, world!", ContentType.Text.Plain)
        }
        route("/message") {
            get {
                call.respond(data)
            }
            post {
                val message = call.receive<Message>()
                data.add(message)
                call.respond(HttpStatusCode.Created)
            }
            delete {
                data.clear()
                call.respond(HttpStatusCode.OK, "Cleared")
            }
        }
    }
}