package org.simple.api

import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.http.ContentType.Application.Json
import io.ktor.serialization.jackson.*
import io.ktor.server.testing.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.simple.api.Message
import org.simple.api.message

class MessageModuleTest {

    @Test
    fun testGet() {
        testApplication {
            application {
                message()
            }
            val response = client.get("/message")
            assertEquals(HttpStatusCode.OK, response.status)
        }
    }

    @Test
    fun testPost() {
        testApplication {
            val client = createClient {
                install(ContentNegotiation) {
                    jackson()
                }
            }
            application {
                message()
            }
            val message = Message("Hello, world!")
            val response = client.post("/message") {
                contentType(Json)
                setBody(message)
            }

            assertEquals(HttpStatusCode.Created, response.status)
        }
    }

    @Test
    fun testDelete() {
        testApplication {
            application {
                message()
            }
            val response = client.delete("/message")
            assertEquals(HttpStatusCode.OK, response.status)
        }
    }
}